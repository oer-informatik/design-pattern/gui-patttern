# Model-View-Presenter (MVP) - Passive View

<span class="hidden-text">
https://oer-informatik.de/gui_pattern_mvp
</span>

> **tl/dr;** _(ca. 7 min Lesezeit): Ist Model-View-Presenter nur ein anderer Name für MVC? Was unterscheidet einen Presenter von einer View? Es gibt zahllose Interpretationen dieser GUI-Pattern. Versuch einer Einordnung._

(Dieser Artikel ist Bestandteil einer Serie von Artikeln zu GUI-Pattern, in der [MVC](https://oer-informatik.de/gui_pattern_mvc), [MVVM](https://oer-informatik.de/gui_pattern_mvvm) und [MVP](https://oer-informatik.de/gui_pattern_mvp) voneinander abgegrenzt werden.)

## Kernaspekte des Patterns

Das MVP-Pattern ist gekennzeichnet von:

- kompletter Entkopplung zwischen _View_ und _Model_

- Kommunikation erfolgt immer über den _Presenter_, der als _Mediator_ (ein Pattern) zwischen den beiden anderen Komponenten vermittelt

- Nutzung v.a. in eventbasierten Frameworks mit asynchroner Aktualisierung von Komponenten.

Die Implementierung einer graphischen Oberfläche nach den GUI-Mustern (MVC/MVP/MVVM) wird von jedem modernen Framework anders realisiert. Zum Teil haben die Rollenzuweisungen, Abhängigkeiten und Nachrichtenflüsse nicht mehr viel mit dem zu tun, was man in ursprünglichen Definitionen MVC/MVP/MVVM zugesprochen hatte.

Auf dieser Seite wird versucht, die ursprünglichen Charakteristika des MVP-Patterns in der Spezialisierung _Passive View_ herauszuarbeiten und gegen MVC abzugrenzen. Damit sollte es möglich sein, die Rollen des jeweiligen Frameworks einzuordnen.

## Hintergrund / Probleme bei frühen MVC-Implementierungen

Historisch hatten sich in der MVC-Implementierung von _Smalltalk_ eine Reihe von Probleme eingeschlichen:

- Zwischen _Model_ und _View_ bestand über das _Observer_-Pattern eine Abhängigkeit. Diese Abhängigkeit war zwar gering (lose Kopplung per _Observer_), kann sich aber negativ auf die Evolvierbarkeit und Testbarkeits des Codes auswirken.
- Der Austausch der _View_ für verschiedene Plattformen war aufgrund dieser Abhängigkeit nicht optimal.
- Für Event-basierte Systeme hatte sich die ursprüngliche MVC-Architektur als nicht optimal herausgestellt.

Die Lösung damals war eine Anpassung im Nachrichenfluss und in den Zuständigkeiten, die heute noch bei AJAX-basierten Frameworks anzutreffen ist:

![](plantuml/MVP-Class-small-mitBeziehungen.png)

## Rolle der _View_:

Die View ist lediglich eine Struktur von GUI-Komponenten (z.B. reines HTML über Template-Engines), sie enthält über den reinen Aufruf der _Presenter_ Methoden hinaus keinerlei Logik. In einigen Frameworks kann die View Logik in Form von _Javascript_-Scripten enthalten, die jedoch im wesentlichen nur den _Presenter_-Aufruf vorbereiten.

In der Literatur finden sich auch Definitionen, die eine _Observer_-Beziehung zwischen _View_- und _Model_ zulassen - zur schärferen Abgrenzung der Varianten wollen wir diese Fälle jedoch nicht betrachten.

![](plantuml/mvp-sequenz.png)

## Presenter

Hier wird auf das Verhalten des Users reagiert (das eigentliche Event-Handling).
Sämtliche Änderungen am _Model_ können über das Design-Pattern _Command_ verpackt werden, um _undo_/_redo_ zu ermöglichen.
Der _Presenter_ sucht eine geeignete _View_ (z.B. das nötige _HTML-Template_), aktualisiert das _Model_, fügt die korrekten Daten in die View ein und sendet alles zusammen wieder an den Client.

## _Model_

Die Komponente _Model_ ist weitgehend identisch zu dem _Model_ in MVC.

## Weitere Realisierungen von MVP:

### Realisierung über Interfaces

Um die Testbarkeit und Austauschbarkeit zu erhöhen können _Model_ und _View_ auch über Interfaces realisiert werden, die im Presenter referenziert werden. Hierdurch können leicht Mock-Objekte an Stelle der View oder des Model mit dem Presenter verknüpft und nur dieser isoliert getestet werden (Unittests).

![](plantuml/MVP-Class-small-mitBeziehungenInterface.png)

### _Passive View_

Die oben geschilderte Variante von MVP wird häufig als _passive View_ bezeichnet, da die gesamte von der _View_ benötigte Logik (zur Konfiguration der GUI-Komponenten) im _Presenter_ abgebildet ist. Die _View_ selbst ist weitestgehend frei von Logik und aktiven Komponenten - daher der Name. Sie hat selbst auch keinerlei Beziehung zum _Model_. Diese Form ist besonders gut testbar.

![](plantuml/mvp-passiveview-class.png)


### _Supervising Controller_ (nach Martin Fowler)

In einer zweiten Variante enthält die _View_ Logik für grundlegende, einfache Konfiguration der GUI-Komponenten - etwa das Datenbinding mit dem _Model_. Der _Presenter_ ist für komplexere Aufgaben zuständig. Die oben genannte Entkopplung _View/Model_ findet so nicht statt, dadurch werden schlankere Presenter (Controller) möglich. Da hierbei die Abgrenzung zu den anderen Pattern schwerer fällt, haben wir das Augenmerk auf die Passive View gelegt.


## Unterschiede zu anderen GUI-Mustern:

- Bei MVC kann jede GUI-Komponente (_Widget_, also im Extremfall jeder Button etc.) über ein eigenes _View/Controller_-Paar verfügen. Bei MVP gibt es nur einen Presenter (auf _Form_-Ebene), der für die Benutzereingaben aller Elemente verantwortlich ist und Anfragen ggf. an weitere Presenter weiterreicht.

- Es gibt keinerlei Abhängigkeiten zwischen _Model_ und _View_. Die Kommunikation zwischen diesen beiden Komponenten verläuft immer über den _Presenter_.

- Im Fall eines _Supervising Controllers_ enthält die _View_ deutlich mehr Logik zur Konfiguration als eine _View_ des MVC-Pattern, bei der Variante _Passive View_ enthält die View hingegen keinerlei Logik.

Zum besseren Verständnis und den unterschieden der einzelnen GUI-Pattern sollten auch die Artikel der anderen Pattern gelesen werden:

- [Das Model-View-Controller-Pattern (MVC)](https://oer-informatik.de/gui_pattern_mvc): Der Ursprung aller modernen GUI-Pattern

- [Das Model-View-Presenter-Pattern (MVP)](https://oer-informatik.de/gui_pattern_mvp):  Häufig in AJAX-basierten Frameworks anzutreffendes, komplett entkoppeltes GUI-Pattern

- [Das Model-View-ViewModel-Pattern (MVVM)](https://oer-informatik.de/gui_pattern_mvvm): Realsierung in vielen modernen Desktop-App Frameworks


## Links und weitere Informationen

- Grundlagenpapier zu MVP von Mike Potel (Taligent / IBM):

  - [MVP: Model-View-Presenter The Taligent Programming Model for C++ and Java (PDF, 1996)](http://www.wildcrest.com/Potel/Portfolio/mvp.pdf)

- Grundlagenpapier des Designteams von Dolphin Smalltalk MVP Andy Bower, Blair McGlashan  (Object Arts Ltd.):

  - [Twisting the Triad, The evolution of the Dolphin Smalltalk MVP (2000)](https://mvc.givan.se/papers/Twisting_the_Triad.pdf)

  -  [Kurzform (Präsentation)](http://esug.org/data/ESUG2000/Slides/twistingTheTriad.pdf)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/design-pattern/gui-pattern](https://gitlab.com/oer-informatik/design-pattern/gui-pattern).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
